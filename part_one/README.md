# Part One - Command line ascii art

## Building and running

### With Docker

There's a Dockerfile in this folder which builds a container that has the
latest GHC as well as the binary for this project pre-compiled. Running the
container drops you in to a bash session in a folder with the app, where
you can then pass arguments to it to test it.

```
docker build -t part_one .
docker run -it part_one
./asciiart   # Inside container now
```

### Otherwise

You'll have to install GHC, which varies from platform to platform, but once
you've got GHC installed just run:

```
ghc asciiart.hs
./asciiart(.exe)
```
