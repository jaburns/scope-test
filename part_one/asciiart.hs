import Data.List
import Data.Maybe
import System.Environment
import Text.Read

type Renderer = Int -> [String]

main = do
    args <- getArgs
    mapM_ putStrLn $ case parseArgs args of
        Just (renderer, height) -> renderer height
        Nothing -> usageLines

-- |Defines the message to print when the program arguments are missing or incorrect.
usageLines :: [String]
usageLines = [
    "Usage: ./asciiart [shape] [count]",
    "  [shape] can be either 'tree' or 'ex'",
    "  [count] is the height of the output in terminal lines"]

-- |Given the program arguments as strings, attempt to get a Renderer and an output height.
parseArgs :: [String] -> Maybe (Renderer, Int)
parseArgs [] = Nothing
parseArgs [_] = Nothing
parseArgs (a:b:xs) = do
    renderer <- findRenderer a
    size <- readMaybe b
    if size > 0 then Just (renderer, size) else Nothing
  where
    findRenderer "tree" = Just treeRenderer
    findRenderer "ex" = Just exRenderer
    findRenderer _ = Nothing

-- |Renders out an xmas tree of some provided height as a list of strings from top to bottom.
treeRenderer :: Renderer
treeRenderer height = map buildRow [1..height]
  where
    buildRow row = let spaces = take (height - row) $ repeat ' '
                       stars = intercalate "-" . take row $ repeat "*"
                   in spaces ++ stars

-- |Renders an X pattern of some provided height as a list of strings from top to bottom.
exRenderer :: Renderer
exRenderer height = take height $ map (buildRow . abs) [-halfHeight..halfHeight]
  where
    halfHeight = height `quot` 2
    spaces = repeat ' '
    buildRow 0 = take halfHeight spaces ++ "X"
    buildRow sep = take (halfHeight - sep) spaces ++ "X" ++ take (2 * sep - 1) spaces ++ "X"
